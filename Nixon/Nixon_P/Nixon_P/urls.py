"""Nixon_P URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib import admin
from django.urls import path
from Nixon_A import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('app_profile/', views.app_profile, name ='app_profile'),
    path('app_widget/', views.app_widget, name='app_widget'),
    path('ui_accordion/', views.ui_accordion, name='ui_accordion'),
    path('ui_alerts/', views.ui_alerts, name='ui_alerts'),
    path('ui_badges/', views.ui_badges, name='ui_badges'),
    path('ui_button/', views.ui_button, name='ui_button'),
    path('ui_dropdown/', views.ui_dropdown, name='ui_dropdown'),
    path('ui_images/', views.ui_images, name='ui_images'),
    path('uc_calendar/', views.uc_calendar, name='uc_calendar'),
    path('uc_carousel/', views.uc_carousel, name='uc_carousel'),
    path('uc_datamap/', views.uc_datamap, name='uc_datamap'),
    path('uc_todo/', views.uc_todo, name='uc_todo'),
    path('uc_scrollable/', views.uc_scrollable, name='uc_scrollable'),
    path('uc_sweetalert/', views.uc_sweetalert, name='uc_sweetalert'),
    path('uc_toastr/', views.uc_toastr, name='uc_toastr'),
    path('uc_bsr/',views.uc_bsr,name='uc_bsr'),
    path('uc_absr/',views.uc_absr,name='uc_absr'),
    path('uc_nestable/',views.uc_nestable, name='uc_nestable'),
    path('uc_portlets/',views.uc_portlets, name='uc_portlets'),
    path('chart_flot/', views.chart_flot, name='chart_flot'),
    path('table_basic/', views.table_basic, name='table_basic'),
    path('icons_themify/', views.icons_themify, name='icons_themify'),
    path('form_basic/', views.form_basic, name='form_basic'),
    path('invoice/', views.invoice, name='invoice'),
    path('invoice_edit/', views.invoice_edit, name='invoice_edit'),
    path('page_login/', views.page_login, name='page_login'),
    path('page_register/', views.page_register, name='page_register'),
    path('page_fpasswd/', views.page_fpasswd, name='page_fpasswd')


]
