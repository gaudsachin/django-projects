from django.apps import AppConfig


class NixonAConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Nixon_A'
