from django.shortcuts import render


# Create your views here.
def index(request):
    return render(request, 'index.html')

def app_profile(request):
    return render(request, 'app-profile.html')

def app_widget(request):
    return render(request, 'app-widget-card.html')

def ui_accordion(request):
    return render(request, 'ui-accordion.html')

def ui_alerts(request):
    return render(request, 'ui-alerts.html')

def ui_badges(request):
    return render(request, 'ui-badges.html')

def ui_button(request):
    return render(request, 'ui-button.html')

def ui_dropdown(request):
    return render(request, 'ui-dropdown.html')

def ui_images(request):
    return render(request, 'ui-images.html')

def uc_calendar(request):
    return render(request, 'uc-calendar.html')

def uc_carousel(request):
    return render(request, 'uc-carousel.html')

def uc_datamap(request):
    return render(request, 'uc-datamap.html')

def uc_todo(request):
    return render(request, 'uc-todo-list.html')

def uc_scrollable(request):
    return render(request, 'uc-scrollable.html')

def uc_sweetalert(request):
    return render(request, 'uc-sweetalert.html')

def uc_toastr(request):
    return render(request, 'uc-toastr.html')

def uc_bsr(request):
    return render(request, 'uc-range-slider-basic.html')

def uc_absr(request):
    return render(request, 'uc-range-slider-advance.html')

def uc_nestable(request):
    return render(request, 'uc-nestable.html')

def uc_portlets(request):
    return render(request, 'uc-portlets.html')

def chart_flot(request):
    return render(request, 'chart-flot.html')

def table_basic(request):
    return render(request, 'table-basic.html')

def icons_themify(request):
    return render(request, 'font-themify.html')

def form_basic(request):
    return render(request, 'form-basic.html')

def invoice(request):
    return render(request, 'invoice.html')

def invoice_edit(request):
    return render(request, 'invoice-editable.html')

def page_login(request):
    return render(request, 'page-login.html')

def page_register(request):
    return render(request, 'page-register.html')

def page_fpasswd(request):
    return render(request, 'page-reset-password.html')
